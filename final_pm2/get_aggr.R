#Data analyses for Krysty PM1 O157:H7 data
library(opm)
library(multcomp)

#get the data and associate it with the metadata file
opm <- read_opm("./",include = "*.csv")

#create metadata file if needed
#metadata <-collect_template(opm,outfile="based_metadata.txt")

meta.opm <- include_metadata(opm,md="pm2_metadata.txt")
agg <- do_aggr(meta.opm,method="opm-fast",boot=100)

auc.fast <- aggregated(agg, "AUC", ci = FALSE)
write.table(auc.fast, file='agg_out.txt', quote=FALSE, sep="\t")
