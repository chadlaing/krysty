README.md

#Metadata
First, the supplied file was incorrect, leading to errors.
A correct metadata file was created using:
    
    #get the data and associate it with the metadata file
    opm <- read_opm("./",include = "*.csv")
    metadata <-collect_template(opm,outfile="based_metadata.txt")

This can be uncommented and run at any time to generate a new metadata file based on all .csv files in the directory.


#Analysis
First, all the 0s in wells 2-96 were converted to 0.01 in the PM1 plates to avoid divide by 0 errors using the command:
    
    perl -pi -e 's/,0,/,0\.01,/g' *.csv  

The PM2 plates did not suffer from this problem, so no conversion was applied.


Second, the group_compare_analysis.R script was invoked on the data from the appropriate directory:

    R --vanilla < group_compare_analysis.R > pm1_results.txt
    R --vanilla < group_compare_analysis.R > pm2_results.txt


Note that the name of the well reported is based on the plate type specified in the line:

    print(paste("Well", wells(aggSubset, plate="PM1", full=TRUE)))

   
#Other
Note that using Rscript instead causes an error! This is a bug in Rscript and took forever to figure out.


#Re-do the following analysis
<!-- #Would it be possible for you to re-run this analysis but with using pH ranging from 3.5-7 so just wells A1-A7.
#Is possible to get an average for each pH for each strain.  
#I’d like to be able to run statistics between strain for each pH to see if there are significant differences.
#(i.e. compare strain 342 pH 3.5 vs strain 219 pH 3.5)  - maybe this can be done in R as well.  -->

##
Modified the krysty_area.txt file by removing all rows past pH7.5






