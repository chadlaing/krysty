#heatmap for Krysty based on Biolog data

library(gplots)
library(RColorBrewer)

#image location
png('area_heatmap_ph35-7.png', width=1000, height=1000)
biologData <- read.delim("krysty_area_mod.txt", header=TRUE, row.names=1)

myPink <- rgb(243,149,149,maxColorValue=255)
myGreen <- rgb(102,205,170,maxColorValue=255)
myYellow <- rgb(255,255,167,maxColorValue=255)

#colors
colors<-colorRampPalette(colors=c(myGreen,myYellow,myPink))(1000)

heatmap.2(as.matrix(biologData),dendrogram="col",
          notecol="black",col=colors,scale="none",key=TRUE, keysize=1.5,
          hclustfun = function(x) hclust(x,method = 'ward.D'),
   		  distfun = function(x) dist(x,method = 'euclidean'),
   		  Rowv=FALSE,
          density.info="none", trace="none", cexRow=1.2,cexCol=1.2)





dev.off()
warnings()

